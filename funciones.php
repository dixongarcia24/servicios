<?php
  require_once("include/Libreria_OO.php");
  class funciones{
    function verificarConexion(){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $aux = "No conectado";
      if ($miConeccion){
        $aux = "Conectado";
        $objetoConeccion->CerrarBD($miConeccion);
      }
      return $aux;
    }

    function iniciarSesion($email,$clave){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = 'select * from t2000063_DB_MP.users where email="'.$email.'" and password="'.sha1($clave).'"';
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $arrayRegistros=array();
        $arrayRegistros = mysql_fetch_array($registros);
        $datos[0]="true";
        $datos[1]=$arrayRegistros['abr'];
        $datos[2]=$arrayRegistros['name'];
        $datos[3]=$arrayRegistros['company'];
        $datos[4]=$arrayRegistros['city'];
        $datos[5]=$arrayRegistros['status'];
        $datos[6]=$arrayRegistros['id'];
        $datos[7]=$arrayRegistros['admin'];
      }else {
        $datos[0] = "false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function verificarRegistro($email){
      $objConexion = new ConeccionBD;
      $miConexion = $objConexion->ConectarBD();
      $tiraSQL = "SELECT * FROM users where email='".$email."'";
      $retorna = $objConexion->EjecutarSQL($tiraSQL,$miConexion);
      if(mysql_num_rows($retorna)){
        return true;
      }else{
        return false;
      }
    }

    function enviarEmail($email,$abr,$nombre,$titulo,$texto,$asunto){
      $head = file_get_contents ("mail/head-mail.html");
      $footer = file_get_contents("mail/footer-mail.html");
      $mensaje = $head.'
                <!-- MODULE ROW // -->
                <tr>
                  <td align="center" valign="top">
                    <!-- CENTERING TABLE // -->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                      <tr>
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                            <tr>
                              <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                  <tr>
                                    <td align="center" valign="top">

                                      <!-- CONTENT TABLE // -->
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                          <td valign="top" class="textContent">
                                            <h1 mc:edit="header" style="color:#5F5F5F;line-height:100%;font-family:Helvetica,Arial,sans-serif;font-size:35px;font-weight:normal;margin-bottom:5px;text-align:left;">'.$abr.' '.$nombre.'</h1><br/>
                                            <h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">'.$titulo.'</h3><br/>
                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">'.$texto.'</div>
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- // CONTENT TABLE -->

                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </table>
                    <!-- // CENTERING TABLE -->
                  </td>
                </tr>
                <!-- // MODULE ROW -->
                '.$footer;

      $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
      $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
      $cabeceras .= 'From: MarianParty <no-reply@marianparty.com>' . "\r\n"."Reply-To: info@marianparty.com" . "\r\n";
      $res = mail($email, $asunto, $mensaje, $cabeceras);
      if($res)
        return true;
      else
        return false;
    }

    function registrarUsuario($abr,$nombre,$email,$tel,$emp,$ciu,$cla){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      if($emp ==""){
        $sql = 'INSERT INTO t2000063_DB_MP.users
                (abr,name,email,city,password,phone) values
                ("'.$abr.'","'.$nombre.'","'.$email.'","'.$ciu.'","'.sha1($cla).'",
                "'.$tel.'")';
      }else{
        $sql = 'INSERT INTO t2000063_DB_MP.users
                (abr,name,email,company,city,password,phone) values
                ("'.$abr.'","'.$nombre.'","'.$email.'","'.$emp.'","'.$ciu.'","'.sha1($cla).'",
                "'.$tel.'")';
      }
      if ($this->verificarRegistro($email)){
        $datos[0]="registrado";
      }else{
        $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
        if ($registros){
          if (mysql_affected_rows($miConeccion)>0){
            $idusuario = mysql_insert_id();
            $cadena = $idusuario.$email.rand(1,9999999).date('Y-m-d');
            $token = sha1($cadena);
            //enlace confirmación
            $sql = 'INSERT INTO t2000063_DB_MP.confirmacion (idusuario,email,token) values
            ("'.$idusuario.'","'.$email.'","'.$token.'")';
            $confirmacion = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
            if ($confirmacion){
              $enlace = $_SERVER["SERVER_NAME"].'/confirmacion.html?idusuario='.sha1($idusuario).'&token='.$token;
            }else{
              $enlace = false;
            }

            //Suscribe
            /*$sql = 'INSERT INTO t2000063_DB_MP.suscriptores
            (email) values
            ("'.$email.'")';
            $objetoConeccion->EjecutarSQL($sql,$miConeccion);*/
            $titulo = "Estás a un paso de ingresar a <strong>MarianParty.com</strong>.";
            $texto = "Confirma tu dirección de correo para poder realizar tus cotizaciones y recibir promociones.<br/><br/>";
            $texto .= '<tr>
                  <td align="center" valign="top">
                    <!-- CENTERING TABLE // -->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr style="padding-top:0;">
                        <td align="center" valign="top">
                          <!-- FLEXIBLE CONTAINER // -->
                          <table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
                            <tr>
                              <td style="padding-top:0;" align="center" valign="top" width="500" class="flexibleContainerCell">

                                <!-- CONTENT TABLE // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="50%" class="emailButton" style="background-color: #3498DB;">
                                  <tr>
                                    <td align="center" valign="middle" class="buttonContent" style="padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;">
                                      <a style="color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;" href="'.$enlace.'" target="_blank">Confirmar mi email</a>
                                    </td>
                                  </tr>
                                </table>
                                <!-- // CONTENT TABLE -->

                              </td>
                            </tr>
                          </table>
                          <!-- // FLEXIBLE CONTAINER -->
                        </td>
                      </tr>
                    </table>
                    <!-- // CENTERING TABLE -->
                  </td>
                </tr>';
            $asunto = "Bienvenido a MarianParty.com! Confirma tu cuenta";
            $SentEmail = $this->enviarEmail($email,$abr,$nombre,$titulo,$texto,$asunto);
            if ($SentEmail)
              $datos[0]="true";
            else
              $datos[0]="no-mail";
          }
          else
            $datos[0]="false";
        } else{
          $datos[0]="false";
        }
      }
      mysql_close();
      return $datos;
    }

    function verificarCaptcha($response){
      $secret = '6LeHdBATAAAAADTiYgbnoLyIpy8AoRRlvF7lLcMD';
      $datos=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response");
      //$datos = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response";
      return $datos;
    }

    function buscarEstados(){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM estados ORDER BY estados.estado ASC";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $datos[0]="true";
        $datos[1]=mysql_num_rows($registros);
        $row = array();
        $i = 2;
        while($row=mysql_fetch_array($registros)){
          $datos[$i]['idestado']=$row['id_estado'];
          $datos[$i]['estado']=$row['estado'];
          $i++;
        }
      }else{
        $datos[0] = "false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function ciudadesDelEstado($estado){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM ciudades WHERE id_estado='".$estado."' ORDER BY ciudades.ciudad ASC";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $datos[0]="true";
        $datos[1]=mysql_num_rows($registros);
        $row = array();
        $i=2;

        while ($row=mysql_fetch_array($registros)) {
          $datos[$i]['idciudad']=$row['id_ciudad'];
          $datos[$i]['ciudad']=$row['ciudad'];
          $i++;
        }
      }
      else
        $datos[0]="false";
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function confirmarCuenta($usuario,$token){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM confirmacion WHERE token='".$token."'";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if (mysql_num_rows($registros)>0){
        $arrayRegistros = array();
        $arrayRegistros = mysql_fetch_array($registros);
        $idusuario = $arrayRegistros['idusuario'];
        $email = $arrayRegistros['email'];
        if(sha1($idusuario)==$usuario){
          $sql = "UPDATE users SET status = 1 WHERE id='".$idusuario."'";
          $result = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
          if($result){
            $sql = "DELETE FROM confirmacion where token='".$token."'";
            $result = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
            if($result){
              $datos[0]="true";
              $sql = "SELECT * FROM users WHERE id=".$idusuario;
              $result = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
              if (mysql_num_rows($result)>0) {
                $array = array();
                $array = mysql_fetch_array($result);
                $abr = $array['abr'];
                $nombre = $array['name'];
                $titulo = "Bienvenido a <strong>MarianParty.com</strong>";
                $texto = "Le damos la más grata bienvenida a nuestro sitio web, ahora podrás adquirir nuestros servicios desde la comodidad de tu hogar u oficina de manera fácil y rápida.<br/><br/>Además podrás recibir boletines informativos de nuestras promociones y eventos públicos a realizar.";
                $asunto = "Bienvenido a MarianParty.com";
                $SentEmail = $this->enviarEmail($email,$abr,$nombre,$titulo,$texto,$asunto);
              }
            }
            else{
              $datos[0]="false";
            }
          }else{
            $datos[0]="NoActivate";
          }
        }else{
          $datos[0]="false";
        }
      }else{
        $datos[0]="false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function linkRestablecer($email){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM users where email='".$email."'";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if (mysql_num_rows($registros)>0){
        $array = array();
        $array = mysql_fetch_array($registros);
        $idusuario = $array['id'];
        $abr = $array['abr'];
        $nombre = $array['name'];
        $cadena = $idusuario.$email.rand(1,9999999).date('Y-m-d');
        $token = sha1($cadena);
        //enlace confirmación
        $sql = 'INSERT INTO t2000063_DB_MP.confirmacion (idusuario,email,token) values
        ("'.$idusuario.'","'.$email.'","'.$token.'")';
        $confirmacion = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
        if ($confirmacion){
          $enlace = $_SERVER["SERVER_NAME"].'/restablecer.html?idusuario='.sha1($idusuario).'&token='.$token;
          $asunto = "Restablecer contraseña en MarianParty.com";
          $titulo = "Restablece contraseña en <strong>MarianParty.com</strong>.";
          $texto = "Para restablecer la contraseña ingresa al enlace e introduzca la nueva contraseña.<br/><br/>Si recibio este correo por error haga caso omiso del mismo.<br/><br/>";
          $texto .= '<tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="padding-top:0;">
                      <td align="center" valign="top">
                        <!-- FLEXIBLE CONTAINER // -->
                        <table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
                          <tr>
                            <td style="padding-top:0;" align="center" valign="top" width="500" class="flexibleContainerCell">

                              <!-- CONTENT TABLE // -->
                              <table border="0" cellpadding="0" cellspacing="0" width="50%" class="emailButton" style="background-color: #3498DB;">
                                <tr>
                                  <td align="center" valign="middle" class="buttonContent" style="padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;">
                                    <a style="color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;" href="'.$enlace.'" target="_blank">Restablecer Contraseña</a>
                                  </td>
                                </tr>
                              </table>
                              <!-- // CONTENT TABLE -->

                            </td>
                          </tr>
                        </table>
                        <!-- // FLEXIBLE CONTAINER -->
                      </td>
                    </tr>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>';
          $SentEmail = $this->enviarEmail($email,$abr,$nombre,$titulo,$texto,$asunto);
          if($SentEmail)
            $datos[0]="true";
          else
            $datos[0]="no-mail";
        }else{
          $datos[0]="false";
        }
      }else{
        $datos[0]="no-user";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function restablecerPassword($usuario,$token,$password){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM confirmacion WHERE token='".$token."'";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if (mysql_num_rows($registros)>0){
        $arrayRegistros = array();
        $arrayRegistros = mysql_fetch_array($registros);
        $idusuario = $arrayRegistros['idusuario'];
        if(sha1($idusuario)==$usuario){
          $sql = "UPDATE users SET password = '".sha1($password)."' WHERE id='".$idusuario."'";
          $result = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
          if($result){
            $sql = "DELETE FROM confirmacion where token='".$token."'";
            $result = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
            if($result){
              $datos[0]="true";
            }
            else{
              $datos[0]="false1";
            }
          }else{
            $datos[0]="noCambio";
          }
        }else{
          $datos[0]="false2";
        }
      }else{
        $datos[0]="false3";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function retornaSuscriptores(){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM suscriptores";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $datos[0]="true";
        $datos[1]=mysql_num_rows($registros);
        $row = array();
        $i=2;
        while ($row=mysql_fetch_array($registros)) {
          $datos[$i] = $row['email'];
          $i++;
        }
      }else{
        $datos[0]="false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function retornaServicios(){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM servicios";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $datos[0]="true";
        $datos[1]=mysql_num_rows($registros);
        $row = array();
        $i=2;
        while ($row=mysql_fetch_array($registros)) {
          $datos[$i]['idservicio'] = $row['id'];
          $datos[$i]['servicio'] = $row['servicio'];
          $i++;
        }
      }else{
        $datos[0]="false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;

    }

    function retornaPrecioServicio($id){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = 'select * from t2000063_DB_MP.servicios where id="'.$id.'"';
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $arrayRegistros=array();
        $arrayRegistros = mysql_fetch_array($registros);
        $datos[0]="true";
        $datos[1]=$arrayRegistros['precio'];
      }else {
        $datos[0] = "false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function retornaImagenesServicios($id){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM servicios where id='".$id."'";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $datos[0]="true";
        $arrayRegistros=array();
        $arrayRegistros = mysql_fetch_array($registros);
        $datos[1]=$arrayRegistros['imagenes'];
        $datos[2]=$arrayRegistros['carpeta'];
      }else{
        $datos[0]="false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function retornarTreeView(){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM servicios ORDER BY categoria";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if (mysql_num_rows($registros)>0) {
        $datos[0]= "true";
        $datos[1]= mysql_num_rows($registros);
        $row = array();
        $i = 2;
        while ($row=mysql_fetch_array($registros)) {
          $datos[$i]["id"] = $row["id"];
          $datos[$i]["servicio"] = $row["servicio"];
          $datos[$i]["categoria"] = $row["categoria"];
          $i++;
        }
      } else
        $datos[0]="false";
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function retornaCategorias(){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT * FROM categoria ORDER BY id";
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if (mysql_num_rows($registros)>0) {
        $datos[0]= "true";
        $datos[1]= mysql_num_rows($registros);
        $row = array();
        $i = 2;
        while ($row=mysql_fetch_array($registros)) {
          $datos[$i]["id"] = $row["id"];
          $datos[$i]["categoria"] = $row["nombre"];
          $i++;
        }
      } else
        $datos[0]="false";
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function retornaUsuario($id){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = 'select * from t2000063_DB_MP.users where id="'.$id.'"';
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $arrayRegistros=array();
        $arrayRegistros = mysql_fetch_array($registros);
        $datos[0]="true";
        $datos[1]=$arrayRegistros['abr'];
        $datos[2]=$arrayRegistros['name'];
        $datos[3]=$arrayRegistros['company'];
        $datos[4]=$arrayRegistros['city'];
        $datos[5]=$arrayRegistros['status'];
        $datos[6]=$arrayRegistros['id'];
        $datos[7]=$arrayRegistros['phone'];
        $datos[8]=$arrayRegistros['email'];
      }else {
        $datos[0] = "false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;

    }

    function retornaPrecioCiudad($id){
      $objetoConeccion = new ConeccionBD;
      $miConeccion = $objetoConeccion->ConectarBD();
      $sql = "SELECT ciudad,precio,estado FROM ciudades,estados where estados.id_estado = ciudades.id_estado and ciudades.id_ciudad = ".$id;
      $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
      if(mysql_num_rows($registros)>0){
        $arrayRegistros = array();
        $arrayRegistros = mysql_fetch_array($registros);
        $datos[0] = "true";
        $datos[1] = $arrayRegistros['ciudad'];
        $datos[2] = $arrayRegistros['estado'];
        $datos[3] = $arrayRegistros['precio'];
      }else{
        $datos[0] = "false";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function cotizar($id,$fecha,$seleccion,$ciudad,$direccion){
      $user = $this->retornaUsuario($id);
      $filas = "";
      if($user[0]=="true"){
        $objetoConeccion = new ConeccionBD;
        $miConeccion = $objetoConeccion->ConectarBD();
        $ubicacion = array();
        $subTotal = 0;
        $ubicacion = $this->retornaPrecioCiudad($ciudad);
        $aux = $direccion.". ".$ubicacion[1]." - ".$ubicacion[2];
        $sql = 'INSERT INTO cotizaciones (id_usuario,fecha_evento,ubicacion) values
        ('.$id.',"'.$fecha.'","'.$aux.'")';
        $confirmacion = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
        if ($confirmacion){
          $idCotizacion = mysql_insert_id();
          for ($i=0; $i < count($seleccion); $i++) {
            $sql = "SELECT * FROM servicios WHERE id = ".$seleccion[$i];
            $registros = $objetoConeccion->EjecutarSQL($sql,$miConeccion);
            if(mysql_num_rows($registros)>0){
              $arrayRegistros = array();
              $arrayRegistros = mysql_fetch_array($registros);
              $subTotal = $subTotal + $arrayRegistros['precio'];
              $filas = $filas.'<tr>';
              $filas = $filas.'<td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;">'.$arrayRegistros['servicio'].'</td>';
              $filas = $filas.'<td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;">'.$arrayRegistros['precio'].'</td>';
              $filas = $filas.'</tr>';
              $sql = "INSERT INTO serviciosxcotizacion (id_cotizacion,id_servicio) values
              (".$idCotizacion.",".$seleccion[$i].")";
              $objetoConeccion->EjecutarSQL($sql,$miConeccion);
            }
          }
          echo $filas;
          $total = $subTotal + $ubicacion[3];
          $email = $user[8];
          $asunto = "Cotización en MarianParty.com";
          $titulo = "Presupuesto de los servicios de <strong>MarianParty.com</strong>.";
          $texto = "A continuación se le muestra una copia del presupuestos para hacer uso de los servicios MarianParty. Para confirmar la cotiación y finiquitar detalles puede contactarse al 0424-5581498 indicando su número de cotización.<br/><br/>";
          $texto .= "<strong>Nro de cotización: </strong>".$idCotizacion.'<br/><br/>';
          $texto .= "<strong>Fecha del Evento: </strong>".$fecha.'<br/><br/>';
          $texto .= "<strong>Ubicación del Evento: <br/></strong>".$direccion.". ".$ubicacion[0]." - ".$ubicacion[1].'<br/><br/>';
          $texto .= '<table style="border-collapse:collapse;border-spacing:0;border-color:#999;margin: 0 auto;">
                      <tr>
                        <th style="padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#2F60AD;">Servicios</th>
                        <th style="padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#2F60AD;"">Precio</th>
                      </tr>'.$filas.'
                      <tr style="border-top:1px dotted #000;">
                        <td style="text-align: right;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;">Sub-total</td>
                        <td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;">'.$subTotal.'</td>
                      </tr>
                      <tr>
                        <td style="text-align: right;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;">Transporte</td>
                        <td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;">'.$ubicacion[2].'</td>
                      </tr>
                      <tr style="border-top:2px solid #000;">
                        <td style="text-align: right;font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;"><strong>Total</strong></td>
                        <td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;">'.$total.'</td>
                      </tr>
                    </table>';
          $SentEmail = $this->enviarEmail($email,$user[1],$user[2],$titulo,$texto,$asunto);
          $asunto = "COPIA - Cotización en MarianParty.com";
          $email = "cotizaciones@marianparty.com";
          $this->enviarEmail($email,$user[1],$user[2],$titulo,$texto,$asunto);
          if($SentEmail)
            $datos[0]="true";
          else
            $datos[0]="no-mail";
        }
      }else{
        $datos[0]="no-user";
      }
      $objetoConeccion->CerrarBD($miConeccion);
      return $datos;
    }

    function actualizarPrecio($id,$precio){
      $objConexion = new ConeccionBD;
      $miConexion = $objConexion->ConectarBD();
      $tiraSQL = "UPDATE servicios SET precio = ".$precio." WHERE id='".$id."'";
      $retorna = $objConexion->EjecutarSQL($tiraSQL,$miConexion);
      if(mysql_affected_rows($miConexion)>0){
        $datos[0]= "true";
      }else{
        $datos[0]= "false";
      }
      $objConexion->CerrarBD($miConexion);
      return $datos;
    }

    function actualizarPrecioTransporte($id,$precio){
      $objConexion = new ConeccionBD;
      $miConexion = $objConexion->ConectarBD();
      $tiraSQL = "UPDATE ciudades SET precio = ".$precio." WHERE id_ciudad=".$id;
      $retorna = $objConexion->EjecutarSQL($tiraSQL,$miConexion);
      if(mysql_affected_rows($miConexion)>0){
        $datos[0]= "true";
      }else{
        $datos[0]= "false";
      }
      $objConexion->CerrarBD($miConexion);
      return $datos;
    }

  }
?>
