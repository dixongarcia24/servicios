<?php
  require_once 'funciones.php';
  class Servicios{
    function servicio00(){
      try{
        $resultado="";
        $objetoDatos = new funciones();
        $resultado = $objetoDatos->verificarConexion();
      } catch (Exception $e){
        $resultado = "No se pudo conectar a la Base de Datos.";
      }
      return $resultado;
    }
    function servicio01($email,$clave){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->iniciarSesion($email,$clave);
        if ($datos[0]=="true"){
          $resultado = $resultado.'{"existe":"'.$datos[0].'",';
          $resultado = $resultado.'"id":"'.$datos[6].'",';
          $resultado = $resultado.'"abr":"'.$datos[1].'",';
          $resultado = $resultado.'"nombre":"'.$datos[2].'",';
          $resultado = $resultado.'"email":"'.$email.'",';
          $resultado = $resultado.'"empresa":"'.$datos[3].'",';
          $resultado = $resultado.'"ciudad":"'.$datos[4].'",';
          $resultado = $resultado.'"admin":"'.$datos[7].'",';
          $resultado = $resultado.'"status":"'.$datos[5].'"}';
        }else {
          $resultado = '{"existe":"'.$datos[0].'"}';
        }

      } catch (Exception $e){
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio02($abr,$nombre,$email,$tel,$emp,$ciu,$cla){
      try{
        $datos = array();
        $resultado= "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->registrarUsuario($abr,$nombre,$email,$tel,$emp,$ciu,$cla);
        $resultado = $resultado.'{"existe":"'.$datos[0].'"}';
      } catch (Exception $e){
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio03($response){
      try{
        $resultado = "";
        $datos = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->verificarCaptcha($response);
        $resultado = $datos;
      } catch(Exception $e){
        $resultado = '{"success": "false"}';
      }
      return $resultado;
    }

    function servicio04(){
      try{
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->buscarEstados();
        if ($datos[0]=="true"){
          $resultado = $resultado.'{"existe":"'.$datos[0].'", "estados":[';
          for ($i=0; $i <($datos[1]) ; $i++) {
            $resultado = $resultado.'{"'.$datos[$i+2]['idestado'].'":"'.$datos[$i+2]['estado'].'"}';
            if($i!=$datos[1]-1){
              $resultado = $resultado.', ';
            }else{
              $resultado = $resultado.']}';
            }
          }
        }else{
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      }catch(Exception $e){
        $resultado = '{"existe":"'.$datos[0].'"}';
      }
      return $resultado;
    }

    function servicio05($estado){
      try{
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->ciudadesDelEstado($estado);
        if ($datos[0]=="true"){
          $resultado = $resultado.'{"existe":"'.$datos[0].'", "ciudades":[';
          for ($i=0; $i < $datos[1]; $i++) {
            $resultado = $resultado.'{"'.$datos[$i+2]['idciudad'].'":"'.$datos[$i+2]['ciudad'].'"}';
            if($i!=$datos[1]-1)
              $resultado = $resultado.', ';
            else
              $resultado = $resultado.']}';
          }
        }else{
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      }catch(Exception $e){
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio06($usuario,$token){
      try{
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->confirmarCuenta($usuario,$token);
        if($datos[0]!="true"){
          $resultado = '{"existe":"'.$datos[0].'"}';
        }else{
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      }catch(Exception $e){
          $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio07($email){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->linkRestablecer($email);
        if ($datos[0]=="true"){
          $resultado = '{"existe":"'.$datos[0].'"}';
        }else{
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio08($usuario,$token,$password){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->restablecerPassword($usuario,$token,$password);
        if ($datos[0]=="true"){
          $resultado = '{"existe":"'.$datos[0].'"}';
        }else{
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio09(){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->retornaSuscriptores();
        if ($datos[0]=="true"){
          $resultado = '{"existe":"'.$datos[0].'","correos":[';
          for ($i=0; $i < $datos[1]; $i++) {
            $resultado=$resultado.'"'.$datos[$i+2].'"';
            if ($i!=$datos[1]-1){
              $resultado = $resultado.', ';
            }else{
              $resultado = $resultado.']}';
            }
          }
        }else{
          $resultado = '{"existe":"false"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio10(){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->retornaServicios();
        if ($datos[0]=="true"){
          $resultado = '{"existe":"'.$datos[0].'","servicios":[';
          for ($i=0; $i < $datos[1]; $i++) {
            $resultado = $resultado.'{"'.$datos[$i+2]['idservicio'].'":"'.$datos[$i+2]['servicio'].'"}';
            if ($i!=$datos[1]-1){
              $resultado = $resultado.', ';
            }else{
              $resultado = $resultado.']}';
            }
          }
        }else{
          $resultado = '{"existe":"false"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio11($id){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->retornaPrecioServicio($id);
        if ($datos[0]=="true") {
          $resultado = $resultado.'{"existe":"'.$datos[0].'",';
          $resultado = $resultado.'"precio":"'.$datos[1].'"}';
        } else {
          $resultado = '{"existe":"false"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio12($id){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->retornaImagenesServicios($id);
        if ($datos[0]=="true"){
          $resultado = '{"existe":"'.$datos[0].'",';
          $resultado = $resultado.'"imagenes":"'.$datos[1].'",';
          $resultado = $resultado.'"carpeta":"'.$datos[2].'"}';
        }else{
          $resultado = '{"existe":"false"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio13(){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->retornarTreeView();
        $data = $objetoDatos->retornaCategorias();
        if ($datos[0]="true" && $data[0]="true") {
          $resultado = "[";
          for ($i=0; $i < $data[1]; $i++) {
            $resultado = $resultado.'{"text":"'.$data[$i+2]["categoria"].'",';
            $resultado = $resultado.'"id":"'.$data[$i+2]["id"].'"';
            $primera = true;
            $aux = $i+1;
            for ($x=0; $x < $datos[1]; $x++) {
              if ($datos[$x+2]["categoria"] == $aux){
                if ($primera){
                  $resultado = $resultado.', "nodes": [';
                  $primera = false;
                }else
                  $resultado=$resultado.', ';
                $resultado = $resultado.'{"text":"'.$datos[$x+2]["servicio"].'",';
                $resultado = $resultado.'"id":"'.$datos[$x+2]["id"].'"}';
              }
            }
            if (!$primera) {
              $resultado = $resultado.']';
            }else{
              $resultado = $resultado.', "nodes": []';
            }
            $resultado = $resultado.'}';
            if ($i != $data[1]-1 ) {
              $resultado = $resultado.", ";
            }else
              $resultado = $resultado."]";
          }
        }else{
          $resultado = '{"'.$datos[0].'":"'.$data[0].'"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }
    function servicio14($id,$fecha,$seleccion,$ciudad,$direccion){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->cotizar($id,$fecha,$seleccion,$ciudad,$direccion);
        if ($datos[0]=="true") {
          $resultado = '{"existe":"'.$datos[0].'"}';
        }else{
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio15($id,$precio){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->actualizarPrecio($id,$precio);
        if ($datos[0]=="true") {
          $resultado = '{"existe":"'.$datos[0].'"}';
        } else {
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio16($id){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->retornaPrecioCiudad($id);
        if ($datos[0]=="true"){
          $resultado = '{"existe":"'.$datos[0].'",';
          $resultado = $resultado.'"ciudad":"'.$datos[1].'",';
          $resultado = $resultado.'"estado":"'.$datos[2].'",';
          $resultado = $resultado.'"precio":"'.$datos[3].'"}';
        }else{
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

    function servicio17($id,$precio){
      try {
        $datos = array();
        $resultado = "";
        $objetoDatos = new funciones();
        $datos = $objetoDatos->actualizarPrecioTransporte($id,$precio);
        if ($datos[0]=="true") {
          $resultado = '{"existe":"'.$datos[0].'"}';
        } else {
          $resultado = '{"existe":"'.$datos[0].'"}';
        }
      } catch (Exception $e) {
        $resultado = '{"existe":"false"}';
      }
      return $resultado;
    }

  }
?>
